<?php
  class Dao {
    private $host = "us-cdbr-iron-east-03.cleardb.net";
    private $db = "heroku_732086aa9ca5815";
    private $user = "b7de3665e0ecc3";
    private $pass = "1bbf2742";
    public function getConnection () {
      return new PDO("mysql:host={$this->host};dbname={$this->db}", $this->user, $this->pass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
    }

    public function checkUserAndPass ($username, $password) {
      $conn = $this->getConnection();
      $getQuery = "SELECT username FROM users WHERE username = :username AND password = :password";
      $q = $conn->prepare($getQuery);
      $q->bindParam(":username", $username);
      $q->bindParam(":password", $password);
      $q->execute();
      return reset($q->fetchAll());
    }

    public function getUserData ($username) {
      $conn = $this->getConnection();
      $getQuery = "SELECT * FROM users_data WHERE username = :username";
      $q = $conn->prepare($getQuery);
      $q->bindParam(":username", $username);
      $q->execute();
      return $q->fetchAll();
    }

    public function addUserData($username, $word, $reading, $definition) {
      //echo "Entering";
      $conn = $this->getConnection();
      //$getQuery = "SELECT * FROM users_data WHERE username = :username";
      //$getQuery = "INSERT INTO users_data (id, username, word, reading, definition) VALUES (:id, :username, :word, :reading, :definition)";
      $getQuery = "INSERT INTO users_data (id, username, word, reading, definition) VALUES (DEFAULT, :username, :word, :reading, :definition)";

      //echo "Going to prepare and insert";
      $q = $conn->prepare($getQuery);
      //$q->bindParam(":id", $id);
      $q->bindParam(":username", $username);
      $q->bindParam(":word", $word);
      $q->bindParam(":reading", $reading);
      $q->bindParam(":definition", $definition);
      $q->execute();
    }

    public function deleteRow($username, $word, $reading, $definition) {
      //echo "Entering";
      $conn = $this->getConnection();
      //$getQuery = "SELECT * FROM users_data WHERE username = :username";
      //$getQuery = "INSERT INTO users_data (id, username, word, reading, definition) VALUES (:id, :username, :word, :reading, :definition)";
      $getQuery = "DELETE FROM users_data WHERE username = :username AND word = :word AND reading = :reading AND definition = :definition";

      //echo "Going to prepare and insert";
      $q = $conn->prepare($getQuery);
      //$q->bindParam(":id", $id);
      $q->bindParam(":username", $username);
      $q->bindParam(":word", $word);
      $q->bindParam(":reading", $reading);
      $q->bindParam(":definition", $definition);
      $q->execute();
    }

    public function inDatabase($username, $word, $reading, $definition) {
      $conn = $this->getConnection();
      //$getQuery = "SELECT * FROM users_data WHERE username = :username";
      //$getQuery = "INSERT INTO users_data (id, username, word, reading, definition) VALUES (DEFAULT, :username, :word, :reading, :definition)";
      $getQuery = "SELECT * FROM users_data WHERE username = :username word = :word reading = :reading definition = :definition";
      $q = $conn->prepare($getQuery);
      //$q->bindParam(":id", $id);
      $q->bindParam(":username", $username);
      $q->bindParam(":word", $word);
      $q->bindParam(":reading", $reading);
      $q->bindParam(":definition", $definition);
      $q->execute();
      /*if(sizeof($q->fetchAll()) == 0) {
        //No results in DB with that data
        return false;
      } else {
        //That result exists in the database
        return true;
      }*/
      return $q->fetchAll();
    }

  /* This plus the class Dao
  public function checkAccount($uname, $psw) {
    $connection = $this->getConnection();
    $sql = $connection->query("SELECT count(email) FROM user WHERE email='".$uname."'");
    $result = $sql->fetch();
    $count = $result[0];
    if($count < 1){
      return FALSE;
    } else {
      if(($this->checkPassword($uname, $psw)) == TRUE){
        $idRow = $connection->query("SELECT id FROM user where email = '".$uname."'");
        $result = $idRow->fetch();
        $_SESSION['id'] = $result[0];
          return TRUE;
      } else {
        return FALSE;
      }
    }
  }

  public function checkPassword($uname, $psw) {
    $connection = $this->getConnection();
    $row = $connection->query("SELECT count(password) FROM user WHERE email = '".$uname."'");
    $result = $sql->fetch();
    $count = $result[0];
    if($count < 1){
      return FALSE;
    } else {
      $idRow = $connection->query("SELECT ");
    }
  }
  */
}
