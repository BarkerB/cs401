<html>

    <head>
        <title>Jisho Memorize</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
        <script>
        function loaded(){
        window.addEventListener
          var kkeys = [],
          konami = "77,69,77,69,83"; //this spells memes
          window.addEventListener("keydown", function(e) {
            kkeys.push(e.keyCode);
            if (kkeys.toString().indexOf(konami) >= 0) {
              // run code here
              $("#block").fadeIn('500');
              $("#block").delay('15000');
              $("#block").fadeOut('5000');
              $("#block2").fadeIn(1500);
              $("#block2").delay('9000');
              $("#block2").fadeOut('5000');
              $("#block3").fadeIn(1200);
              $("#block3").delay('13000');
              $("#block3").fadeOut('5000');
              $("#block4").fadeIn(400);
              $("#block4").delay('14000');
              $("#block4").fadeOut('5000');
              $("#block5").fadeIn(750);
              $("#block5").delay('12000');
              $("#block5").fadeOut('5000');
            }
          }, true);
        }
        </script>
    </head>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:600,700" rel="stylesheet">
    <link rel="icon" href="favicon.ico" type="image/x-icon"/>

    <body onload="loaded()">
        <img id="block" src="http://i.imgur.com/oZXdQdV.gif" />
        <img id="block2" src="http://i.imgur.com/uOq5Vqy.gif" />
        <img id="block3" src="https://media.giphy.com/media/dbtDDSvWErdf2/giphy.gif" />
        <img id="block4" src="https://media.giphy.com/media/s0z685OFhWyuk/giphy.gif" />
        <img id="block5" src="http://mikeaparicio.com/images/fullstackdeveloper.gif" />

        <div class="wrapper">


            <ul>
                <li><a href="index.php">HOME</a></li>
                <li><a href="about.php" class='active-page'>ABOUT</a></li>
                <li style="float:right"><a href="login.php">ACCOUNT</a></li>
            </ul>

            <div class=headingTitle>
                <h1>辞書 MEMORIZE</h1>
            </div>

            <div class=styledSearch>
              <form action="index.php" method="get">
                  <input type="text" name="search" autocomplete="off" placeholder="Search with romaji, kana, or English..." required>
                  <!--<<input type="button" value="Search"> -->
                  <input type="submit" value="Search">
              </form>
            </div>

            <div class=aboutParagraph>

                <p>Jisho Memorize is a tool that allows users to search Japanese words using either romaji, kana, or English translations to find various information
                about the word including definition, kanji, and written kana. Users can directly save words to their account which provides a neat list of words for
                studying later.</p>
                <br></br>
                <p>This site uses the JMdict, Kanjidic2, JMnedict and Radkfile dictionary files. These files are the property of the Electronic Dictionary Research and Development Group, and are used in conformance with the Group's licence. </p>
                <br></br>
                <p>Special thanks to Jisho.org for providing the API to make this site possible.</p>

            </div>

            <div class="push"></div>
        </div>
        <footer class="footer">Copyright &copy; JishoMemorize.com</footer>
    </body>
</html>
