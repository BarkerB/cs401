<?php
	require_once "Dao.php";
	session_start();
?>

<html>

    <head>
        <title>Jisho Memorize</title>
    </head>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:600,700" rel="stylesheet">
    <link rel="icon" href="favicon.ico" type="image/x-icon"/>

    <body>
    <div class="wrapper">


        <ul>
            <li><a href="index.php">HOME</a></li>
            <li><a href="about.php">ABOUT</a></li>
            <li style="float:right"><a href="login.php" class='active-page'>ACCOUNT</a></li>
        </ul>



        <div class=headingTitle>
            <h1>辞書 MEMORIZE</h1>
        </div>

        <?php
          //Must be logged in to see this page
          if(isset($_SESSION["authed_user"])){
              header('Location: account.php');
          }

        ?>


        <form method="POST" action="handler.php">

          <div class="container">
            <input type="text" placeholder="Enter Username" name="username" value="<?php echo $_SESSION['username'];?>" required>
						<br></br>
            <input type="password" placeholder="Enter Password" name="password" required>

            <input type="submit" name="login" value="Login">
          </div>
        </form>

				<?php
				if(isset($_SESSION['invalid'])){
					echo "<div class=errorMessage>";
					echo "<p>Invalid login credentials!</p>";
					echo "</div>";
					unset($_SESSION['invalid']);
					unset($_SESSION['username']);
				}
				?>

        <div class="push"></div>
    </div>
    <footer class="footer">Copyright &copy; JishoMemorize.com</footer>
    </body>
</html>
