<html>

    <head>
        <title>Jisho Memorize</title>
    </head>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:600,700" rel="stylesheet">
    <link rel="icon" href="favicon.ico" type="image/x-icon"/>

    <body>
        <div class="wrapper">


            <ul>
                <li><a href="index.php">HOME</a></li>
                <li><a href="about.php">ABOUT</a></li>
                <li><a href="contact.php">CONTACT</a></li>
                <li style="float:right"><a href="login.php">ACCOUNT</a></li>
            </ul>

            <div class=headingTitle>
                <h1>辞書 MEMORIZE</h1>
            </div>

            <div class=styledSearch>
                <form>
                    <input type="text" placeholder="Search with romaji, kana, or English..." required>
                    <input type="button" value="Search">
                </form>
            </div>

            <div class=aboutParagraph>

                <p>Coming Soon!!!</p>

            </div>

            <div class="push"></div>
        </div>
        <footer class="footer">Copyright &copy; JishoMemorize.com</footer>
    </body>
</html>
