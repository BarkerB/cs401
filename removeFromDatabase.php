<?php
	require_once "Dao.php";
  session_start();
  $db = new Dao();

  //echo "Word: " . $_GET['word'];
  //echo "Reading: " . $_GET['reading'];
  //echo "Definition: " . $_GET['definition'];
  //echo $_GET['username'];
  //echo $_SESSION["username"];
  //if DB has connection
  if( $db->getConnection() ){
    $db->deleteRow($_SESSION["username"], $_GET['word'], $_GET['reading'], $_GET['definition']);
  }
  header('Location: ' . $_SERVER['HTTP_REFERER']);
  exit;
?>
