<?php
	require_once "Dao.php";
  session_start();
  function parseInput($input) {
    if(isset($_SESSION["authed_user"])){
      $db = new Dao();
      $inDBArray = $db->getUserData($_SESSION["authed_user"]);
      $resultsArrayLength = sizeof($inDBArray);
    }


    //$searchValue = $_GET["search"]
    $inputEncoded = $encoded = rawurlencode($input); //We raw URL encode so that it can handle spaces
    $homepage = file_get_contents('http://jisho.org/api/v1/search/words?keyword='.$inputEncoded);
    $noResultsMessage = "{\"meta\":{\"status\":200},\"data\":[]}";

    if($homepage == $noResultsMessage){
			echo "<div class=infoMessage>";
				echo "<p>Yoinks! No results were returned for the search: " . $input . "</p>";
			echo "</div>";

      return;
    }

    $pieces = explode("dbpedia\":", $homepage);

    echo "<tr>\n";
    if(isset($_SESSION["authed_user"])){
        echo "  <td>Save</td>\n";
    }
    echo "  <td>Word</td>\n";
    echo "  <td>Reading</td>\n";
    echo "  <td>Definition</td>\n";
    echo "</tr>\n";

    for($i = 0; $i < sizeof($pieces)-1; $i++){
      $exists = false;
      $startPos = strpos($pieces[$i], "word\":\"")+7;
      $endPos = strpos($pieces[$i], "\",\"", $startPos);
      $piecesWord = substr($pieces[$i], $startPos, ($endPos-$startPos));

      $startPos = strpos($pieces[$i], "reading\":\"")+10;
      $endPos = strpos($pieces[$i], "\"", $startPos);
      $piecesReading = substr($pieces[$i], $startPos, ($endPos-$startPos));
      if (strlen($piecesWord) > 80) { //Test case was 643 characters, so 80 can be increased if necessary
        //TODO: Make this elegant... This case happens if the word is purely Katakana!
        $piecesWord = $piecesReading;
      }

      $startPos = strpos($pieces[$i], "english_definitions\":[")+22;
      $endPos = strpos($pieces[$i], "]", $startPos);
      $piecesDefinition = substr($pieces[$i], $startPos, ($endPos-$startPos));
      $piecesDefinition = str_replace("\"", "", $piecesDefinition);
      $piecesDefinition = str_replace(",", ", ", $piecesDefinition);

      $startPos = strpos($pieces[$i], "parts_of_speech\":[")+18;
      $endPos = strpos($pieces[$i], "]", $startPos);
      $piecesSpeech = substr($pieces[$i], $startPos, ($endPos-$startPos));
      $piecesSpeech = str_replace("\"", "", $piecesSpeech);
      $piecesSpeech = str_replace(",", ", ", $piecesSpeech);

      //echo "Word: " . $piecesWord . " Reading: " . $piecesReading . " Definition: " . $piecesDefinition . " Part of Speech: " . $piecesSpeech . "\n";
      echo "<tr>\n";
      if(isset($_SESSION["authed_user"])){
        for($q = 0; $q < ($resultsArrayLength); $q++){
          if($inDBArray[$q][2] == $piecesWord){
            $exists = true;
            break;
          }
        }

        if($exists == true){
          echo "  <td><form action=\"removeFromDatabase.php\">
                  <input type=\"hidden\" name=\"word\" value=\"$piecesWord\">
                  <input type=\"hidden\" name=\"reading\" value=\"$piecesReading\">
                  <input type=\"hidden\" name=\"definition\" value=\"$piecesDefinition\">
                  <input type=\"submit\" name=\"addToDb\" value=\"Remove\">
                  </form></td>\n";
        } else {

          echo "  <td><form action=\"addToDatabase.php\">
                  <input type=\"hidden\" name=\"word\" value=\"$piecesWord\">
                  <input type=\"hidden\" name=\"reading\" value=\"$piecesReading\">
                  <input type=\"hidden\" name=\"definition\" value=\"$piecesDefinition\">
                  <input type=\"submit\" name=\"addToDb\" value=\"Save\">
                  </form></td>\n";
        }
      }

      //<form action="index.php" method="post">
      //    <input type="submit" value="Search">
      //</form>
      echo "  <td>".$piecesWord."</td>\n";
      echo "  <td>".$piecesReading."</td>\n";
      echo "  <td>".$piecesDefinition."</td>\n";
      echo "</tr>\n";
    }

    /*$startPos = strpos($pieces[0], "word\":\"")+7;
    $endPos = strpos($pieces[0], "\",\"", $startPos);
    $piecesWord = substr($pieces[0], $startPos, ($endPos-$startPos));
    echo "\n";
    echo $piecesWord;

    $startPos = strpos($pieces[0], "reading\":\"")+10;
    $endPos = strpos($pieces[0], "\"", $startPos);
    $piecesReading = substr($pieces[0], $startPos, ($endPos-$startPos));
    echo "\n";
    echo $piecesReading;

    $startPos = strpos($pieces[0], "english_definitions\":[")+22;
    $endPos = strpos($pieces[0], "]", $startPos);
    $piecesDefinition = substr($pieces[0], $startPos, ($endPos-$startPos));
    echo "\n";
    $piecesDefinition = str_replace("\"", "", $piecesDefinition);
    echo $piecesDefinition;

    $startPos = strpos($pieces[0], "parts_of_speech\":[")+18;
    $endPos = strpos($pieces[0], "]", $startPos);
    $piecesSpeech = substr($pieces[0], $startPos, ($endPos-$startPos));
    echo "\n";
    $piecesSpeech = str_replace("\"", "", $piecesSpeech);
    echo $piecesSpeech;*/
    return;
  }
?>
