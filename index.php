<html>

    <head>
        <title>Jisho Memorize</title>
        <script type="text/javascript" src="https://cdn.rawgit.com/mikeflynn/egg.js/master/egg.min.js"></script>
        <script type="text/javascript">
        var egg = new Egg("up,up,down,down,left,right,left,right,b,a", function() {
          jQuery('#egggif').fadeIn(500, function() {
            window.setTimeout(function() { jQuery('#egggif').hide(); }, 5000);
          });
          }).listen();
        </script>

        <style type="text/css">
          #egggif {position: absolute; top: 33%; left: 25%; display: none; }
        </style>
    </head>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:600,700" rel="stylesheet">
    <link rel="icon" href="favicon.ico" type="image/x-icon"/>
    <img id="egggif" src="http://media.giphy.com/media/4hnQDVKVARZ6w/giphy.gif" />


    <body>
    <div class="wrapper">


        <ul>
            <li><a href="index.php" class='active-page'>HOME</a></li>
            <li><a href="about.php">ABOUT</a></li>
            <li style="float:right"><a href="login.php">ACCOUNT</a></li>
        </ul>



        <div class=headingTitle>
            <h1>辞書 MEMORIZE</h1>
        </div>

        <div class=styledSearch>
            <form action="index.php" method="get">
                <input type="text" name="search" autocomplete="off" placeholder="Search with romaji, kana, or English..." required>
                <!--<<input type="button" value="Search"> -->
                <input type="submit" value="Search">
            </form>
        </div>

        <div class=resultsTable>
            <table id="printedTable">

              <?php

                if (isset($_GET['search'])) {
                  require_once("parseresults.php");
                  parseInput($_GET['search']);

                }else{
                  //echo "No search variable!";
                }
              ?>
            </table>

        </div>
        <div class="push"></div>
    </div>
    <footer class="footer">Copyright &copy; JishoMemorize.com</footer>
    </body>
</html>
