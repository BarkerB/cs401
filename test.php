<html>

    <head>
        <title>Jisho Memorize</title>
    </head>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:600,700" rel="stylesheet">
    <link rel="icon" href="favicon.ico" type="image/x-icon"/>

    <body>
    <div class="wrapper">


        <ul>
            <li><a href="index.html">HOME</a></li>
            <li><a href="about.html">ABOUT</a></li>
            <li><a href="contact.html">CONTACT</a></li>
            <li style="float:right"><a href="account.html">ACCOUNT</a></li>
            <li>
                <a href='login.html' class='Login icon' title='Login'></a>
            </li>
        </ul>



        <div class=headingTitle>
            <h1>辞書 MEMORIZE</h1>
        </div>

        <div class=styledSearch>
            <form action="test.php" method="get">
                <input type="text" name="search" autocomplete="off" placeholder="Search with romaji, kana, or English..." required>
                <!--<<input type="button" value="Search"> -->
                <input type="submit" value="Search">
            </form>
        </div>



        <div class=resultsTable>
            <table id="printedTable">

              <?php

                if (isset($_GET['search'])) {
                  require_once("parseresults.php");
                  parseInput($_GET['search']);

                }else{
                  //echo "No search variable!";
                  echo "<p>Enter a search term in the box above to search for results!</p>";
                }
              ?>

            </table>

        </div>

        <div class="push"></div>
    </div>
    <footer class="footer">Copyright &copy; JishoMemorize.com</footer>
    </body>
</html>
