<?php
	require_once "Dao.php";
  //require_once("parseresults.php");
	session_start();
	$db = new Dao();
	$accountName = $_SESSION["authed_user"];
?>

<html>

    <head>
        <title>Jisho Memorize</title>
    </head>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:600,700" rel="stylesheet">
    <link rel="icon" href="favicon.ico" type="image/x-icon"/>

    <body>
        <div class="wrapper">


            <ul>
                <li><a href="index.php">HOME</a></li>
                <li><a href="about.php">ABOUT</a></li>
                <li style="float:right"><a href="account.php" class='active-page'>ACCOUNT</a></li>
                <li>
                    <a href='/default.asp' class='account icon' title='Account'></a>
                </li>
            </ul>

            <div class=headingTitle>
                <h1>辞書 MEMORIZE</h1>
            </div>


            <?php
              //Must be logged in to see this page
	            if(!isset($_SESSION["authed_user"])){
		              $_SESSION["Unauthorized"] = "Must be logged in to see the previous Page!";
                  header('Location: login.php');
	            } else {
                //echo "Welcome " . $_SESSION["authed_user"];
              }
	          ?>

            <div class=styledSearch>
                <form id="logout" action="logout.php">
                    <input type="submit" value="Logout!">
                </form>
            </div>

            <div class=savedTable>
                <p>Your Saved Words</p>
                <table id="printedTable">
                    <tr>
                        <th>Word</th>
                        <th>Reading</th>
                        <th>Definition</th>
                    </tr>

                    <?php
											header('content-type: text/html; charset=utf-8');
                      //Must be logged in to see this page
                      if(isset($_SESSION["authed_user"])){
                        $result = $db->getUserData($_SESSION["authed_user"]);
                        //print_r($result);
                        //echo "Data type: " . gettype($result);
                        include("printTable.php");
                        //echo "About to call printTable";
                        printUserTable($result);
												//echo "About to add";
												//addUserData('72', 'brandonbarker@u.boisestate.edu', '辞書', 'じしょ', 'Defintion');
												//echo "added?";
                      }
                    ?>

                </table>

            </div>

            <div class="push"></div>
        </div>
        <footer class="footer">Copyright &copy; JishoMemorize.com</footer>
    </body>
</html>
